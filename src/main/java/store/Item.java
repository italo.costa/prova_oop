package store;

public class Item {
    private String title;
    private String publisher;
    private String yearPublished;
    private String isbn;




    public void display(){
        System.out.println(
                "Item{" +
                "title='" + title + '\'' +
                ", publisher='" + publisher + '\'' +
                ", yearPublished='" + yearPublished + '\'' +
                ", isbn='" + isbn + '\'' +
                '}');
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getYearPublished() {
        return yearPublished;
    }

    public void setYearPublished(String yearPublished) {
        this.yearPublished = yearPublished;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
}
