package store;

public class Book extends Item {
    private String author;
    private String edition;
    private String volume;


    @Override
    public void display() {
        System.out.println("Book{" +
                "author='" + author + '\'' +
                ", edition='" + edition + '\'' +
                ", volume='" + volume + '\'' +
                '}');
    }


    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }
}
