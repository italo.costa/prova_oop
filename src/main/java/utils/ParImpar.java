package utils;


public class ParImpar {
    public EnumParImpar  type;

    public ParImpar(int number) {
        if (number % 2 == 0){
            setType(EnumParImpar.PAR);
            System.out.println(number + " é PAR");
        } else{
            setType(EnumParImpar.IMPAR);
            System.out.println(number + " é Ímpar");
        }
    }

    public EnumParImpar getType() {
        return type;
    }

    public void setType(EnumParImpar type) {
        this.type = type;
    }
}
